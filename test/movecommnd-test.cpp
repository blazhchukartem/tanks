#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <vector>

#include "move_command.h"
#include "mocks/movable.h"

TEST(MoveCommand, normal) {
    MockMovable <std::vector<int>> mockMov;

    std::vector<int> pos = {100, 100};

    EXPECT_CALL(mockMov, setPosition(pos)).Times(testing::AtLeast(1));
    MoveCommand<std::vector<int>> mc = MoveCommand<std::vector<int>>(mockMov, pos);
    mc.Execute();
}