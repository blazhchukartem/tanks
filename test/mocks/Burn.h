#pragma once

#include <gmock/gmock.h>

#include "command.h"
class MockBurn: public Burn {
public:
    MOCK_METHOD(void, BurnFuel, (), (override));
};