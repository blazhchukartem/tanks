#pragma once

#include <gmock/gmock.h>

#include "command.h"
class MockFuel: public Fuel {
public:
    MOCK_METHOD(bool, FuelTrue, (), (override));
};
