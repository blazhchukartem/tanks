#pragma once

#include <gmock/gmock.h>

#include "command.h"

template <class Vector>
class MockMovable: public Movable <Vector> {
    public:
        MOCK_METHOD(Vector, getPosition, (), (const, override));
        MOCK_METHOD(void, setPosition, (Vector const& newValue), (override));
        MOCK_METHOD(Vector, getVelocity, (), (const, override));
};