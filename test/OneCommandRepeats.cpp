#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "move_command.h"
#include "vector"
#include "mocks/movable.h"
#include "mocks/Fuel.h"
#include "mocks/Burn.h"
#include "ReturnCommand.h"
//Передвинуть танк, если нет топлива, то танк не едет
//Одна команда повторяется в очереди игры


TEST(OneCommandRepeats, normal) {
    std::vector<Command*> vectorMacroCommand;
    MockMovable<std::vector<int>> mockMovable;
    MockBurn mockBurn;
    MockFuel mockFuel;
    std::vector<int> position = {100,100};
    MoveCommand<std::vector<int>>* moveCommand = new MoveCommand<std::vector<int>>(mockMovable,position);
    BurnCommand* burnCommand = new BurnCommand(mockBurn);
    FuelCommand* fuelCommand = new FuelCommand(mockFuel);
    MacroCommand* macroCommand = new MacroCommand(vectorMacroCommand);
    Queue* mainQueue = new Queue();
    ReturnCommand* returnCommand = new ReturnCommand(macroCommand, mainQueue, 3);

    macroCommand->Add(fuelCommand);
    macroCommand->Add(moveCommand);
    macroCommand->Add(burnCommand);
    macroCommand->Add(returnCommand);

    mainQueue->Add(macroCommand);

    EXPECT_CALL(mockMovable, setPosition(position)).Times(testing::AtLeast(3));
    EXPECT_CALL(mockBurn, BurnFuel()).Times(testing::AtLeast(3));
    EXPECT_CALL(mockFuel, FuelTrue()).Times(testing::AtLeast(3));

    mainQueue->Execute();

}

TEST(OneCommandRepeats, outOfFuel) {
    std::vector<Command*> vectorMacroCommand;
    MockMovable<std::vector<int>> mockMovable;
    MockBurn mockBurn;
    MockFuel mockFuel;
    std::vector<int> position = {100,100};
    MoveCommand<std::vector<int>>* moveCommand = new MoveCommand<std::vector<int>>(mockMovable,position);
    BurnCommand* burnCommand = new BurnCommand(mockBurn);
    FuelCommand* fuelCommand = new FuelCommand(mockFuel);
    MacroCommand* macroCommand = new MacroCommand(vectorMacroCommand);
    Queue* mainQueue = new Queue();
    ReturnCommand* returnCommand = new ReturnCommand(macroCommand, mainQueue, 2);

    macroCommand->Add(fuelCommand);
    macroCommand->Add(moveCommand);
    macroCommand->Add(burnCommand);
    macroCommand->Add(returnCommand);

    mainQueue->Add(macroCommand);
    ON_CALL(mockFuel, FuelTrue()).WillByDefault(testing::Return(false));
    ON_CALL(mockFuel, FuelTrue()).WillByDefault(testing::Return(true));



    EXPECT_CALL(mockFuel, FuelTrue())
    .Times(testing::AtLeast(2))
    .WillOnce(testing::Return(true))
    .WillOnce(testing::Return(false));

    mainQueue->Execute();

}