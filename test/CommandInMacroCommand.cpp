#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "move_command.h"
#include "vector"
#include "mocks/movable.h"
#include "mocks/Fuel.h"
#include "mocks/Burn.h"
#include "ReturnCommand.h"
//Передвинуть танк, если нет топлива, то танк не едет
//Команды внутри макрокоманды повторяются заданное кол-во раз
TEST(CommandInMacrocommand, normal) {
    std::vector<Command*> vectorMacroCommand;
    MockMovable<std::vector<int>> mockMovable;
    MockBurn mockBurn;
    MockFuel mockFuel;
    std::vector<int> position = {100,100};
    MoveCommand<std::vector<int>>* moveCommand = new MoveCommand<std::vector<int>>(mockMovable,position);
    BurnCommand* burnCommand = new BurnCommand(mockBurn);
    FuelCommand* fuelCommand = new FuelCommand(mockFuel);
    MacroCommand* macroCommand = new MacroCommand(vectorMacroCommand, 2);
    Queue* mainQueue = new Queue();
    ReturnCommand* returnCommand = new ReturnCommand(macroCommand, mainQueue, 3);

    macroCommand->Add(fuelCommand);
    macroCommand->Add(moveCommand);
    macroCommand->Add(burnCommand);
    macroCommand->Add(returnCommand);

    mainQueue->Add(macroCommand);

    EXPECT_CALL(mockMovable, setPosition(position)).Times(testing::AtLeast(6));
    EXPECT_CALL(mockBurn, BurnFuel()).Times(testing::AtLeast(6));
    EXPECT_CALL(mockFuel, FuelTrue()).Times(testing::AtLeast(6));

    mainQueue->Execute();

}