#pragma once

class CommandException{}; 
class Command {
   public:
      virtual void Execute() = 0; 
};

class ReadPropertyException {};
class ChangePropertyException {};

template<class Vector> class Movable {
    public:
      virtual Vector getPosition() const = 0;
      virtual void setPosition(Vector const& newValue) = 0;
      virtual Vector getVelocity() const = 0;
};

template<class Float> class Rotable{
public:
    virtual Float getAngle() const = 0;
    virtual void setAngle(Float const& newValue) = 0;
};

class Burn{
public:
    virtual void BurnFuel() = 0;
};

class Fuel{
public:
    virtual bool FuelTrue() = 0;
};

