#pragma once

#include "command.h"
#include "Queue.h"
#include "vector"

template <class Vector>
class MoveCommand: public Command {
    Movable<Vector> &m_obj;
    const Vector m_newPos;
    public:
        MoveCommand(Movable<Vector> &obj, const Vector& newPos):
            m_obj(obj), m_newPos(newPos){}

        void Execute() {
            m_obj.setPosition(m_newPos);
        }
};

template <class Float>
class RotableCommand: public Command{
    Rotable<Float> &rotableObject;
    const Float rotableNewAngle;
public:
    RotableCommand(Rotable<Float>& obj, const Float& newAngle):rotableObject(obj), rotableNewAngle(newAngle){}
    void Execute(){
        rotableObject.setAngle(rotableNewAngle);
    }
};

class MacroCommand:public Command{
private:
    std::vector<Command*> command;
    int count = 0;
public:
    MacroCommand(std::vector<Command*> command_, int count_ = 1) : command(command_), count(count_) {}
    void Execute() {
        int tempCount = count;
        while(tempCount != 0){
            for(int i = 0; i < command.size();i++){
                command[i]->Execute();
            }
            tempCount--;
        }
        tempCount = count;
    }
    void Add(Command* command_){
        command.push_back(command_);
    }
};

class BurnCommand:public Command{
    Burn &obj;
public:
    BurnCommand(Burn &object):obj(object){}
    void Execute(){
        obj.BurnFuel();
    }
};

class FuelCommand:public Command{
    Fuel &obj;
public:
    FuelCommand(Fuel &object):obj(object){}
    void Execute(){
        obj.FuelTrue();
    }
};

