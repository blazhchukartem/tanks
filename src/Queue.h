#pragma once
#include "command.h"
#include "queue"

class Queue{
    std::queue<Command*> queueCommand;
public:
    void Add(Command *command){
        queueCommand.push(command);
    }

    void Execute(){
        while(!queueCommand.empty()){
            queueCommand.front()->Execute();
            queueCommand.pop();
        }
    }
};