#pragma once

#include "move_command.h"
#include "Queue.h"

class ReturnCommand:public Command{
    Command* cmd;
    Queue* queue;
    int count;
public:
    ReturnCommand(Command* cmd, Queue* queue, int count = 1):cmd(cmd), queue(queue), count(count){}
    void Execute(){
        if(count != 0){
            queue->Add(cmd);
            count --;
        }

    }
};